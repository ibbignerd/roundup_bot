# Adapted by /u/ibbignerd from /u/GoldenSights's ReplyBot

import Colorer
import praw
import yaml

import datetime
from html import unescape
import json
import logging
import msvcrt
import pprint
import sys
import sqlite3
import traceback
import time
import urllib.request

'''USER CONFIGURATION'''
USERNAME = '######'
# Username of the bot
PASSWORD = '######'
# Password of the bot
VERSION = '######'
# Version number of bot
USERAGENT = '######'
# Unique identifier of the bot.
MAXPOSTS = 100
# This is how many posts you want to retrieve all at once. PRAW can
# get 100 at a time.
WAIT = 30
# This is how many seconds you will wait between cycles. The bot is
# completely inactive during this time.
SUBREDDIT = '######'
# This is the sub or list of subs to scan for new posts. For a single sub,
# use 'sub1'. For multiple subreddits, use 'sub1+sub2+sub3+...'
RECIPIENT = '######'
# The username that will receive this PM. It can be the same as USERNAME
# if you want to
MATITLE = '######'
# This will be the title of the PM that you get
MSTITLE = '######'
# Title in message where title contains TITLESTRING
TITLESTRING = ['######']
# Strings in title that should flag a response
PARENTSTRING = ['######', '######']
# These are the words that you are looking for in a comment
REPLYSTRING = '######'
# Basic response for comment flagged and added thread
SUBREPLYSTRING = '######'
# These are messages that are sent to replies
FOOTER = '######'
# This is the footer that goes at the bottom of every comment (Requires
# FOOTER + link + ')')
IGNOREUSER = ['######']
# Users that cannot comment interact with the bot
COMMENTREPLY = ['######']
# Array of replies that can be called. Clearner code than putting long
# replies below
BADWORDS = ['######']
'''All done!'''

# INFO: Returns: [08-05 12:07:53] [LEVEL ] message
FORMAT = '[%(asctime)-13s] [%(levelname)-6s] %(message)s'
DATE_FORMAT = '%m-%d %H:%M:%S'
formatter = logging.Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
handler = logging.StreamHandler()
handler.setFormatter(formatter)
log = logging.getLogger(__name__)
# INFO: Set logging level: INFO, DEBUG, WARNING, ERROR
log.setLevel(logging.DEBUG)
log.addHandler(handler)

# Load YAML doc into dict
loc = str("C:\\Users\\Nathan\\Desktop\\RedditPython\\RoundUp_bot\\mydata.txt")
f=open(loc)
dict = iter(yaml.load(f).items())

# INFO: Assign variables to values from YAML file mydata.txt
for key, value in dict:
    if 'USERNAME' == key:
        USERNAME = value
    elif 'PASSWORD' == key:
        PASSWORD = value
    elif 'VERSION' == key:
        VERSION = value
    elif 'USERAGENT' == key:
        USERAGENT = value + VERSION
    elif 'RECIPIENT' == key:
        RECIPIENT = value
    elif 'MATITLE' == key:
        MATITLE = value
    elif 'SUBREDDIT' == key:
        SUBREDDIT = value
    elif 'PARENTSTRING' == key:
        PARENTSTRING = value
    elif 'MAXPOSTS' == key:
        MAXPOSTS = value
    elif 'WAITSHORT' == key:
        WAITSHORT = value
    elif 'WAITLONG' == key:
        WAITLONG = value
    elif 'REPLYSTRING' == key:
        REPLYSTRING = value
    elif 'SUBREPLYSTRING' == key:
        SUBREPLYSTRING = value
    elif 'FOOTER' == key:
        FOOTER = value
    elif 'IGNOREUSER' == key:
        IGNOREUSER = value
    elif 'TITLESTRING' == key:
        TITLESTRING = value
    elif 'MSTITLE' == key:
        MSTITLE = value
    elif 'COMMENTREPLY' == key:
        COMMENTREPLY = value
    elif 'BADWORDS' == key:
        BADWORDS = value
f.close()

# INFO: Connect to sql.db file (create if doesn't exist)
sql = sqlite3.connect('sql.db')
log.info('Loaded SQL Database')
# INFO: Create cursor to carry out SQL arguments
cur = sql.cursor()

# INFO: Create
cur.execute('CREATE TABLE IF NOT EXISTS lastpost(ID TEXT, SUBS TEXT)')
cur.execute('CREATE TABLE IF NOT EXISTS saveposts(ID TEXT, SUB TEXT, TYPE TEXT, TITLE TEXT, DATE TEXT, DESC TEXT)')
cur.execute('CREATE TABLE IF NOT EXISTS subscriptions(ID TEXT)')
log.info('Loaded Completed table')

sql.commit()

# INFO: Create Reddit object through praw
r = praw.Reddit(USERAGENT)
# INFO: Log into Reddit using the USERNAME and PASSWORD variables
r.login(USERNAME, PASSWORD)


# INFO: Search subreddit comments
def scanSubComments():
    subreddits = SUBREDDIT.split('+')

    log.info('Searching ' + SUBREDDIT + ' comments')
    for sub in subreddits:
        for line in cur.execute("SELECT * FROM lastpost WHERE SUBS = \'" + sub +"\'"):
            mindate = line[0]

        request = 'http://api.redditanalytics.com/getRecent.php?limit=400&subreddit=' + sub + '&mindate=' + str(mindate)
        log.debug(request)
        response = urllib.request.urlopen(request)
        str_response = response.readall().decode('utf-8')
        allComments = json.loads(str_response)
        try:
            for comment in allComments['data']:
                ignore = False
                cBody = comment['body']
                cBody_low = cBody.lower()
                commentTime = comment['created_utc']
                if any(key.lower() in cBody_low for key in PARENTSTRING):
                    log.warning('Contains keyword: ' + cBody_low)
                    objComment =r.get_info(thing_id=str(comment['name']))

                    cLink = objComment.submission.permalink
                    print(cLink)
                    # http://www.reddit.com/r/jailbreak//2cl60t/thread_title/cd1s5e
                    cLink_array = cLink.split('/')
                    # INFO: Last section
                    cLink_array[8] = ''
                    cLink_array[7] = ''
                    cLink_array[5] = ''
                    # http://www.reddit.com/r/jailbreak/comments/2cl60t/thread_title/
                    csLinklong = '/'.join(cLink_array)
                    csLink1 = csLinklong.replace('//', '/')
                    csLink = csLink1.replace('/', '//', 1)
                    csTitle = unescape(comment['link_title'])
                    cFooter = str(FOOTER) + csLink + ') ^' + str(VERSION)

                    cAuthor = comment['author']

                    c = r.get_info(thing_id=comment['name'])
                    if '[roundup' in csTitle.lower():
                        c.reply(COMMENTREPLY[0] + cFooter)

                    elif '[request' in csTitle.lower():
                        c.reply(COMMENTREPLY[1] + cFooter)

                    elif '/u/roundupbot' in cBody_low:
                        try:
                            c.reply(COMMENTREPLY[3] + cFooter)
                        except Exception:
                            continue
                    elif any(badWord.lower() in cBody_low for badWord in BADWORDS):
                        c.reply(COMMENTREPLY[4] + cFooter)
                        # INFO: Don't reply to self or banned users
                    elif cAuthor != USERNAME and any(bannedUser.lower() not in cAuthor.lower() for bannedUser in IGNOREUSER):
                        if 'add' in cBody_low and 'roundup' in cBody_low:
                            cur.execute('SELECT * FROM saveposts WHERE ID=?', [csLink])

                            if not cur.fetchone():
                                if "]" in csTitle:
                                    sqlTitle = csTitle.split("]")
                                    sqlTitle[0] += "]"
                                elif ")" in csTitle:
                                    sqlTitle = csTitle.split(")")
                                    sqlTitle[0] = sqlTitle[0].replace("(", "[") + "]"
                                else:
                                    sqlTitle = ['[Thread]', csTitle]

                                sqlMainTitle = sqlTitle[1].lstrip(' ')
                                params = (csLink, cLink_array[4], sqlTitle[0], sqlMainTitle, returnDateTime("date"), "")
                                cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)
                                log.warning(
                                    'Adding link from ' + cAuthor + ' to saveposts: ' + csLink)

                                if RECIPIENT != cAuthor:
                                    r.send_message(RECIPIENT, MATITLE, '**RoundUp_bot**\n\n' + cAuthor +
                                                   ' has said one of your keywords.\n\n[' +
                                                   csTitle + '.](' + csLink + ')')
                                    c.reply(REPLYSTRING + cFooter)
                                else:
                                    c.reply(
                                        'Your wish is my command, master!' + cFooter)
                                sql.commit()
                            else:
                                try:
                                    c.reply(COMMENTREPLY[2] + cFooter)
                                except Exception:
                                    continue
                        else:
                            r.send_message(RECIPIENT, 'Mentioned in comment', cBody + "\n\n" + csLink)
                    else:
                        log.warning('Will not reply to self or banned users or deleted comments: ' + cAuthor)
                        log.info('Continuing to rest of comments in queue')
                        # INFO: Add to lastpost
                exe = "DELETE FROM lastpost WHERE SUBS = \'" + sub + "\'"
                cur.execute(exe)
                params = (commentTime, sub)
                cur.execute("INSERT INTO lastpost VALUES(?,?)", params)
                sql.commit()

        except KeyError: #Called when there are no new comments
            #print(traceback.format_exc())
            continue
    log.info('Done with comments')


# INFO: Search subreddit titles
def scanSubTitles():
    log.info('Searching ' + SUBREDDIT + ' titles')
    subreddit = r.get_subreddit(SUBREDDIT)
    submissions = subreddit.get_new(limit=MAXPOSTS)
# http://www.reddit.com/r/redditdev/comments/2f11b6/is_there_a_way_to_get_a_users_comments_based_on_a/
    for sub in submissions:
        sid = sub.id
        sTitle = unescape(sub.title)
        sLink = sub.permalink
        if any(skey.lower() in sTitle.lower() for skey in TITLESTRING):
            try:
                sAuthor = sub.author.name
            except AttributeError:
                sAuthor = '[DELETED]'
            sLink_array = sLink.split('/')
            sLink_array[8] = ''
            sLink_array[7] = ''
            sLink_array[5] = ''
            sLinklong = '/'.join(sLink_array)
            sLink1 = sLinklong.replace('//', '/')
            sLink = sLink1.replace('/', '//', 1)

            cur.execute('SELECT * FROM saveposts WHERE ID=?', [sLink])
            if not cur.fetchone():
                sqlTitle = sTitle.split("]")
                sqlTitle[0] += "]"
                sqlMainTitle = sqlTitle[1].lstrip(' ')

                params = (sLink, sLink_array[4], sqlTitle[0], sqlMainTitle, returnDateTime("date"), "")
                cur.execute('INSERT INTO saveposts VALUES(?,?,?,?,?,?)', params)
                log.warning('Adding link from ' + sAuthor + ' to saveposts: ' + sLink)
                #sub.add_comment(SUBREPLYSTRING + FOOTER + sLink + ') ^' + VERSION)
                if RECIPIENT != sAuthor:
                    r.send_message(RECIPIENT, MSTITLE, '**RoundUp_bot**\n\n' + sAuthor +
                                   ' has used one of your keywords in their submission title.\n\n[' +
                                   sub.title + '](' + sLink + ')')
                sql.commit()
        sql.commit()
    log.info('Done with submission titles')

def scanPM():
    log.info('Searhing Inbox.')
    pms = r.get_unread(update_user=True, limit=100)
    for pm in pms:
        pid = pm.id
        author = pm.author.name
        pbody = pm.body
        if not pm.was_comment:
            if author == RECIPIENT or author == "hbfamaila":
                log.warning(author + ' send the command: ' + pbody)
                pbodyl = pbody.lower()
                if pbodyl == "help":
                    response = ("##Help Output\n\n"
                                "1. isAlive - Returns \"I am alive\"\n"
                                "2. return \n"
                                "    1. [subreddit] all - Return all threads in 'saveposts'\n"
                                "    2. [subreddit] [days of the week] - Returns threads for dates specified\n"
                                "3. remove [submission id] - Remove thread from 'saveposts'\n"
                                "4. die - end program")
                    pm.reply(response)
                elif pbodyl == "isalive":
                    pm.reply("I am alive")
                elif pbodyl == "die":
                    pm.reply("Shutting down")
                    sql.close()
                    sys.exit()
                elif pbodyl.startswith("return"):
                    pbodyl = pbodyl.replace("all", "monday tuesday wednesday thursday friday saturday sunday")
                    log.debug("Starting return command for " + pbodyl)
                    response = str(pbody + ":\n\n")
                    response += generateRoundUp(pbodyl, "null")
                    pm.reply(response)
                elif pbodyl.startswith("remove"):
                    look = pbodyl.split(" ")
                    for l in range(len(look)):
                        if l == 0:
                            continue
                        params = 'DELETE FROM saveposts WHERE ID LIKE \'%' + look[l] + '%\''
                        cur.execute(params)
            else:
                if "subscribe" in pm.subject.lower() or "subscription" in pm.subject.lower():
                    if "add" in pbody.lower():
                        if "iosthemes" in pbody.lower():
                            cur.execute('INSERT INTO subscriptions VALUES(?,?)', [author, "iosthemes"])
                            pm.reply("You have been added to the /r/iOSthemes mailing list.\n\nTo remove yourself, just send me another message with the subject of \"subscribe\" and the body as \"remove\"")
                        else:
                            cur.execute('INSERT INTO subscriptions VALUES(?,?)', [author, "jailbreak"])
                            pm.reply("You have been added to the /r/jailbreak mailing list.\n\nTo remove yourself, just send me another message with the subject of \"subscribe\" and the body as \"remove\"")
                        log.info("Adding " + author + " to the subscriptions database")
                    elif "remove" in pbody.lower():
                        log.info("Removing " + author + " from the subscriptions database")
                        cur.execute('DELETE FROM subscriptions WHERE ID = \'' + author + '\'')
                        pm.reply("You have been removed to the mailing list.\n\nTo add yourself back again, just send me another message with the subject of \"subscribe\" and the body as \"add\"")
                else:
                    r.send_message(RECIPIENT, 'Forwarded private message from /u/' + author, pbody)
                    pm.reply("Hey. You just sent me a message, but I'm just a bot. I forwarded your message on to /u/ibbignerd already. You should contact him if you have any questions or whatnot. \n\nIf you were trying to subscribe to the daily messages, check out the [wiki](http://www.reddit.com/r/ibbignerd_/wiki/roundup_bot).\n\nHave a great day!")
                    log.warning('Forwarded private message to ' + RECIPIENT)

        else:
            if pm.subject == "post reply":
                r.send_message(RECIPIENT, 'Forwarded post reply by /u/' + author, '[' + pm.link_title + '](' + pm.context +')\n\n' + pbody)
                log.warning('Forwarded post reply to ' + RECIPIENT)
            elif pm.subject == "comment reply":
                r.send_message(RECIPIENT, 'Forwarded comment reply by /u/' + author, '[' + pm.link_title + '](' + pm.context +')\n\n' + pbody)
                log.warning('Forwarded comment reply to ' + RECIPIENT)
        pm.mark_as_read()
    log.info('Done with Inbox')

def generateRoundUp(inputStr, vartype):
    look = inputStr.split(" ")
    lookCount = 1
    output = ""
    for l in range(len(look)):
        if l == 0:
            continue
        if l == 1:
            continue
        if look[l] == "monday":
            ago = " - 7 days ago"
        if look[l] == "tuesday":
            ago = " - 6 days ago"
        if look[l] == "wednesday":
            ago = " - 5 days ago"
        if look[l] == "thursday":
            ago = " - 4 days ago"
        if look[l] == "friday":
            ago = " - 3 days ago"
        if look[l] == "saturday":
            ago = " - 2 days ago"
        if look[l] == "sunday":
            ago = " - 1 day ago"
        params = 'SELECT * FROM saveposts WHERE DATE LIKE \'' + look[l] + '%\' AND SUB LIKE \'' + look[1] + '\''
        print(params)
        sqlCount = 1
        for line in cur.execute(params): #Wednesday, August 06
            if sqlCount == 1:
                date_array = line[4].split(" ")
                dateFinal = date_array[2].lstrip("0")
                if dateFinal[-1:] == 1:
                    dateFinal += "^st"
                if dateFinal[-1:] == 2:
                    dateFinal += "^nd"
                if dateFinal[-1:] == 3:
                    dateFinal += "^rd"
                else:
                    dateFinal += "^th"
                date_array[2] = dateFinal
                date = " ".join(date_array)
                if vartype == "mail":
                    output += "---------------\n\n##" + date + "\n\n"
                else:
                    output += "---------------\n\n##" + date + ago +"\n\n"
            output += str(sqlCount) + ".[" + line[2] + " **"+ line[3] + "**](" + line[0] + ") - \n\n"
            sqlCount += 1
    return output

def sendMail():
    log.debug("Sending mailing list")
    rdate = returnDateTime("date")
    date = rdate.split(",")
    returnText = str("return jailbreak " + date[0])
    body = generateRoundUp(returnText, "mail")
    returnText1 = str("return jailbreak " + date[0])
    body1 = generateRoundUp(returnText, "mail")

    params = 'SELECT * FROM subscriptions'
    for line in cur.execute(params):
        if line[0].startswith("sent"):
            continue
        if line[1].startswith("jailbreak"):
            try:
                r.send_message(line[0], "Daily RoundUp", body)
            except:
                log.warning(line[0] + " does not exist")
        else:
            try:
                r.send_message(line[0], "Daily RoundUp", body1)
            except:
                log.warning(line[0] + " does not exist")
    log.debug("Done sending mailing list")

def readInput(timeout):
    start_time = time.time()
    input = ''
    while True:
        if msvcrt.kbhit():
            chr = msvcrt.getche()
            if ord(chr) == 13: # enter_key
                break
            elif ord(chr) >= 32: #space_char
                input = chr.decode("utf-8")
                if input == "q":
                    sql.close()
                    sys.exit()
        if len(input) == 0 and (time.time() - start_time) > timeout:
            break

    print('')
    if len(input) > 0:
        return input

def returnDateTime(inputStr):
    if inputStr == "date":
        today = datetime.date.today()
        # INFO: Tuesday, August 05
        output = today.strftime('%A, %B %d')
    elif inputStr == "time":
        local = time.localtime()
        output = time.strftime('%H', local)
    elif inputStr == "full":
        local = time.localtime()
        output = time.strftime('%M', local)
    return output

while True:
    hr = int(returnDateTime("time"))
    if hr > 22 or hr < 8:
        WAIT = WAITLONG
    else:
        WAIT = WAITSHORT
    minute = int(returnDateTime("full"))
    fullTime = str(hr) + str(minute)
    fullDate = returnDateTime("date")
    last = ""
    for line in cur.execute('SELECT ID FROM subscriptions WHERE ID LIKE \'sent:%\''):
        last = str(line).split(":")
    try:
        scanSubComments()
        scanPM()
        scanSubTitles()
        if int(fullTime) > 2347 and int(fullTime) < 2359 and last[0] != fullDate:
            sendMail()
            cur.execute('UPDATE subscriptions SET ID = \'sent:' + fullTime + '\' WHERE ID LIKE \'sent:%\'')
    except Exception:
        print(traceback.format_exc())
    sql.commit()
    print('Running again in ' + str(WAIT) + ' seconds; Press \'q\' to exit')
    readInput(WAIT)
