# README #

### Description ###

This is a Reddit bot that searches a specified sub (currently /r/jailbreak, /r/iOSthemes, and /r/ibbignerd_) for specified strings in the title and top level comments. Once it finds a match, it adds it to an SQL database and informs the user that posted.

### Dependencies ###

* PRAW
* Colorer.py
* PyYAML

For questions, please message /u/ibbignerd on Reddit
